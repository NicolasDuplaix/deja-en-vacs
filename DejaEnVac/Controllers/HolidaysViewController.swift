//
//  HolidaysViewController.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import UIKit
import Alamofire

class HolidaysViewController: UIViewController {

    // Outlets
    @IBOutlet weak var listHolidays: UITableView!
    
    //properties
    var holidays: [HolidaysResponseHoliday] = []
    var country: String = ""
    var countryCode: String = ""
    var year: String = ""
    let cellSpacing: CGFloat = 20
    
    // Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Vacances"
        
        listHolidays.dataSource = self
        listHolidays.delegate = self

        listHolidays.rowHeight = UITableView.automaticDimension
        listHolidays.estimatedRowHeight = 200
        
        self.loadHolidaysData()
    }
    
    // Api Call
    private func loadHolidaysData() {
        let parameters: Parameters = [
            "api_key": "209a28f24db3dc00c092d0852732611712440638",
            "country": countryCode,
            "year": year
        ]

        AF.request("https://calendarific.com/api/v2/holidays", method: .get, parameters: parameters)
            .validate(statusCode: [200])
            .responseDecodable(of: HolidaysResponse.self) {[weak self] resp in
                switch resp.result {
                case .success(let holidaysResponse):
                    //Attribuer à un datasource local
                    //Mettre à jour mon tableau
                    self?.holidays = holidaysResponse.response.holidays
                    self?.listHolidays.reloadData()
                case .failure(let aferror):
                    print(aferror.localizedDescription)
                }
            }
    }
}

extension HolidaysViewController: UITableViewDelegate {
    
}

extension HolidaysViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        holidays.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacing
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let holiday = holidays[indexPath.row]
        
        let cell = listHolidays.dequeueReusableCell(withIdentifier: "holidayCellId", for: indexPath) as? HolidayTableViewCell
        
        cell?.nameLabel.text = holiday.name
        cell?.descriptionLabel.text = holiday.description
        cell?.dateLabel.text = holiday.date.iso
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let holiday = holidays[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "formId") as? FormViewController
        
        if let vc = vc {
            vc.date = holiday.date.iso
            vc.holidayName = holiday.name
            vc.country = country
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
