//
//  CountriesViewController.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import UIKit
import Alamofire

class CountriesViewController: UIViewController {
    // Outlets
    @IBOutlet weak var countriesField: UITextField!
    @IBOutlet weak var yearsField: UITextField!
    @IBOutlet weak var countryStackView: UIStackView!
    @IBOutlet weak var yearStackView: UIStackView!
    @IBOutlet weak var selectTravelButton: UIButton!
    @IBOutlet weak var ErrorLabel: UILabel!
    
    
    // Properties
    let countryPicker = UIPickerView()
    let yearPicker = UIPickerView()
    
    var countries: [CountryResponseInsideCountries] = []
    let years: [String] = Array(2022...2030).map { "\($0)" }
    
    // LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Destination"
        
        countriesField.inputView = countryPicker
        yearsField.inputView
         = yearPicker
        
        countryPicker.delegate = self
        yearPicker.delegate = self
        
        // style
        Commons.borderRadius(element: countryStackView)
        Commons.borderRadius(element: yearStackView)
        Commons.borderRadius(element: selectTravelButton)
        
        self.loadCountries()
        
    }
    
    //Actions
    @IBAction func goToTravelSelectionAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "holidaysID") as? HolidaysViewController
        
        guard let country = countriesField.text, let year = yearsField.text else {
            Commons.addErrorLabel(errorLabel: ErrorLabel)
            return
        }
        
                if (!country.isEmpty && !year.isEmpty) {
            vc?.country = countriesField.text ?? ""
            vc?.countryCode = countries.first(where: {$0.countryName == countriesField.text})?.countryCode ?? ""
            vc?.year = yearsField.text ?? ""
            if let vc = vc {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            Commons.addErrorLabel(errorLabel: ErrorLabel)
        }
    }
    
    // Api Call
    private func loadCountries() {
        let parameters: Parameters = ["api_key": "209a28f24db3dc00c092d0852732611712440638"]

        AF.request("https://calendarific.com/api/v2/countries", method: .get, parameters: parameters)
            .validate(statusCode: [200])
            .responseDecodable(of: CountryResponse.self) {[weak self] resp in
                switch resp.result {
                case .success(let countryResponse):
                    //Attribuer à un datasource local
                    //Mettre à jour mon tableau
                    self?.countries = countryResponse.response.countries
                case .failure(let aferror):
                    print(aferror.localizedDescription)
                }
            }
    }
}

extension CountriesViewController: UIPickerViewDelegate {
    
}

extension CountriesViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerView == yearPicker ? years.count : countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerView == yearPicker ? years[row] : countries[row].countryName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView == yearPicker) {
            yearsField.text = years[row]
        } else {
            countriesField.text = countries[row].countryName
        }
    }
}
