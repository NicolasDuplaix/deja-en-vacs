//
//  FormViewController.swift
//  DejaEnVac
//
//  Created by etudiant on 19/01/2022.
//

import UIKit
import MessageUI

class FormViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var stackLastName: UIStackView!
    @IBOutlet weak var stackFistName: UIStackView!
    @IBOutlet weak var stackAge: UIStackView!
    @IBOutlet weak var stackDesc: UIStackView!
    @IBOutlet weak var stackButton: UIButton!
    @IBOutlet weak var addImage: UIButton!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var imagePreview: UIImageView!
    
    // Properties
    var country: String = ""
    var date: String = ""
    var holidayName: String = ""
    var image: UIImage?

    //Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Coordonées"

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        Commons.borderRadius(element: stackLastName)
        Commons.borderRadius(element: stackFistName)
        Commons.borderRadius(element: stackAge)
        Commons.borderRadius(element: stackDesc)
        Commons.borderRadius(element: stackButton)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //functions
    func addErrorLabel() {
        errorLabel.text = "Veuillez remplir tous les champs du formulaire avant d'envoyer"
    }
    
    // actions
    @IBAction func addImageAction(_ sender: Any) {
        ImagePickerManager().pickImage(self){ image in
            self.image = image
            self.imagePreview.image = image
            self.imagePreview.isHidden = false
        }
    }
    
    @IBAction func sendEmailAction(_ sender: Any) {
        guard let firstName = firstNameField.text,
                let lastName = lastNameField.text,
                let age = ageField.text,
                let description = descriptionField.text
        else {
            Commons.addErrorLabel(errorLabel: errorLabel)
            return
        }
        
        //check required fields
        if (
            !firstName.isEmpty
            && !lastName.isEmpty
            && !age.isEmpty
            && !description.isEmpty
        ) {
            if MFMailComposeViewController.canSendMail() {
                let composer = MFMailComposeViewController()
                composer.mailComposeDelegate = self
                composer.setToRecipients(["ccmdns@yopmail.com"])
                composer.setSubject("Deja en vac, prépare ton voyage")
                composer.setMessageBody("<p>Nom: \(lastNameField?.text ?? "")</p><p>Prénom: \(firstNameField?.text ?? "")</p><p>Age: \(ageField?.text ?? "")</p><p>Description: \(descriptionField?.text ?? "")</p><p>Nom du voyage: \(holidayName)</p><p>Date du voyage: \(date)</p><p>Pays du voyage: \(country)</p>", isHTML: true)
                
                // add image if exist
                if nil != self.image {
                    let imageData: NSData = (imagePreview.image!).jpegData(compressionQuality: 1.0)! as NSData
                    
                    
                    composer.addAttachmentData(imageData as Data, mimeType: "image/jpeg", fileName: "imageSent.jpeg")
                }
                
                present(composer, animated: true)
            }
        } else {
            Commons.addErrorLabel(errorLabel: errorLabel)
        }
    }

}

extension FormViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


