//
//  ViewController.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import UIKit

class ViewController: UIViewController {

    
    //Outlets
    @IBOutlet weak var choisiTonVoyage: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    //Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "Déjà en vacs"
        
        Commons.borderRadius(element: stackView)
    }

    // Actions
    @IBAction func goToTravelViewAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "selectCountryID")
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

