//
//  HolidaysResponseInside.swift
//  DejaEnVac
//
//  Created by etudiant on 19/01/2022.
//

import Foundation

struct HolidaysResponseInside: Codable {
    let holidays: [HolidaysResponseHoliday]
}
