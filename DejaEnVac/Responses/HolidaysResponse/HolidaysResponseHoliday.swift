//
//  HolidaysResponseHoliday.swift
//  DejaEnVac
//
//  Created by etudiant on 19/01/2022.
//

import Foundation

struct HolidaysResponseHoliday: Codable {
    let name: String
    let description: String
    let date: DateResponse
}
