//
//  CountryMetaResponse.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import Foundation

struct CountryMetaResponse: Codable {
    let code: Int
}
