//
//  countryResponseInsideCountries.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import Foundation

struct CountryResponseInsideCountries: Codable {
    let countryName: String
    let countryCode: String
    
    enum CodingKeys: String, CodingKey {
        case countryName = "country_name"
        case countryCode = "iso-3166"
    }
}
