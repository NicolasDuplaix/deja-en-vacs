//
//  CountryResponse.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import Foundation

struct CountryResponse: Codable {
    let meta: CountryMetaResponse
    let response: CountryResponseInside
}
