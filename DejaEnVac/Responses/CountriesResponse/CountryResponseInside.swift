//
//  CourtyResponseInside.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import Foundation

struct CountryResponseInside: Codable {
    let url: String
    let countries: [CountryResponseInsideCountries]
}
