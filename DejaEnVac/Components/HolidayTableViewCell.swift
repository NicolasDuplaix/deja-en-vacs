//
//  HolidayTableViewCell.swift
//  DejaEnVac
//
//  Created by etudiant on 18/01/2022.
//

import UIKit

class HolidayTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    
    //Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.font = .systemFont(ofSize: 17, weight: .bold)
        nameLabel.backgroundColor = UIColor(named: "secondary")
        nameLabel.textColor = UIColor(named: "primary")
        
        descriptionLabel.backgroundColor = UIColor(named: "primary")
        descriptionLabel.textColor = UIColor(named: "secondary")
        
        dateLabel.backgroundColor = UIColor(named: "primary")
        dateLabel.textColor = UIColor(named: "secondary")
        
        // we don't use the Commons borderRadius function because here, there is only the top border rounded or only the bottom border, not both
        let maskPathName = UIBezierPath(roundedRect: nameLabel.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10.0, height: 10.0))
        
        let shapeName = CAShapeLayer()
        shapeName.path = maskPathName.cgPath
        nameLabel.layer.mask = shapeName
        
        
        let maskPathDate = UIBezierPath(roundedRect: dateLabel.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 10.0, height: 10.0))
        
        let shapeDate = CAShapeLayer()
        shapeDate.path = maskPathDate.cgPath
        dateLabel.layer.mask = shapeDate
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
