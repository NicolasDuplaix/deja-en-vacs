//
//  Commons.swift
//  DejaEnVac
//
//  Created by etudiant on 20/01/2022.
//

import Foundation
import UIKit

class Commons {
    static func borderRadius(element: UIView) {
        let maskPath = UIBezierPath(roundedRect: element.bounds, byRoundingCorners: [.bottomLeft,.bottomRight, .topLeft, .topRight], cornerRadii: CGSize(width: 10.0, height: 10.0))
        
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        element.layer.mask = shape
    }
    
    static func formatDateForViews(originalDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: originalDate) ?? Date()
        
        return dateFormatter.string(from: date)
    }
    
    static func addErrorLabel(errorLabel: UILabel) {
        errorLabel.text = "Veuillez remplir tous les champs du formulaire avant d'envoyer"
    }
}
